import java.util.Arrays;
import java.util.Random;

public class NodNoc {
    public static int getNod(int n1, int n2) {
        int nod = 1;
        for (int i = 1; i <= n1 && i <= n2; i++) {
            if (n1 % i == 0 && n2 % i == 0) {
                nod = i;
            }
        }
        return nod;
    }

    public static int getNoc(int n1, int n2) {
        return n1 * n2 / getNod(n1, n2);

    }

    public static int getNod(int n1, int n2, int n3, int n4) {
        int nod = 1;
        for (int i = 1; i <= n1 && i <= n2 && i <= n3 && i <= n4; i++) {
            if (n1 % i == 0 && n2 % i == 0 && n3 % i == 0 && n4 % i == 0) {
                nod = i;
            }
        }
        return nod;
    }

    public static int getNoc(int n1, int n2, int n3) {
        int n = getNoc(n1, n2);
        return getNoc(n, n3);

    }

    public static int getFactorial(int n) {
        int t = 1;
        for (int i = 1; i <= n; i++) {
            t = t * i;

        }
        return t;
    }

    public static int getFactorialSum() {
        int result = 0;
        for (int i = 1; i <= 9; i++) {
            if (i % 2 != 0) {
                result = result + getFactorial(i);
            }
        }
        return result;
    }

    public static int getPenult(int n) {
        Random r = new Random();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = r.nextInt(100);

        }
        System.out.println(Arrays.toString(arr));
        return Arrays.stream(arr).sorted().skip(n-2).findFirst().getAsInt();
    }
    public static double getPenultDooble(int n) {
        Random r = new Random();
        double[] arr = new double[n];
        for (int i = 0; i < n; i++) {
            arr[i] = r.nextFloat(100);

        }
        System.out.println(Arrays.toString(arr));
        return Arrays.stream(arr).sorted().skip(n-2).findFirst().getAsDouble();
    }
    public static int getPenultNegative(int n) {
        Random r = new Random();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = r.nextInt(100)-100;

        }
        System.out.println(Arrays.toString(arr));
        return Arrays.stream(arr).sorted().skip(n-2).findFirst().getAsInt();
    }
    public static void main(String[] args) {
        System.out.println(getNod(9, 12));
        System.out.println(getNoc(9, 12));
        System.out.println(getNod(15, 25, 30, 35));
        System.out.println(getNoc(9, 12, 18));
        System.out.println(getFactorial(9));
        System.out.println(getFactorialSum());
        System.out.println(getPenult(6));
        System.out.println(getPenultDooble(6));
        System.out.println(getPenultNegative(6));
    }
}


